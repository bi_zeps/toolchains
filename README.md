[![Pipeline Master](https://img.shields.io/gitlab/pipeline/bi_zeps/toolchains/master?label=master&logo=gitlab)](https://gitlab.com/bi_zeps/toolchains)
[![License](https://img.shields.io/badge/dynamic/json?color=orange&label=license&query=%24.license.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F18136148%3Flicense%3Dtrue)](https://gitlab.com/bi_zeps/toolchains/-/blob/master/LICENSE)
[![Open Issues](https://img.shields.io/badge/dynamic/json?color=yellow&logo=gitlab&label=open%20issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F18136148%2Fissues_statistics)](https://gitlab.com/bi_zeps/toolchains/-/issues)
[![Last Commit](https://img.shields.io/badge/dynamic/json?color=green&logo=gitlab&label=last%20commit&query=%24[:1].committed_date&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F18136148%2Frepository%2Fcommits%3Fbranch%3Dmaster)](https://gitlab.com/bi_zeps/toolchains/-/commits/master)

[![gccWin](https://badgen.net/badge/project/gccWin/orange?icon=gitlab)](https://gitlab.com/bi_zeps/toolchains/-/blob/master/README.md#gccWin)
[![Stable Version](https://img.shields.io/docker/v/bizeps/gccWin/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/bi_zeps/toolchains/-/blob/master/CHANGELOG.md#gccWin)
[![Docker Pulls](https://badgen.net/docker/pulls/bizeps/gccWin?icon=docker&label=pulls)](https://hub.docker.com/r/bizeps/gccWin)
[![Docker Image Size](https://badgen.net/docker/size/bizeps/gccWin/stable?icon=docker&label=size)](https://hub.docker.com/r/bizeps/gccWin)

# gccWin
Image prepared for crosscompilation for Windows containing mingw, make and cmake.

`docker run --rm bizeps/gccWin [cmake | make] --help` or run `x86_64-w64-mingw32-*` directly.
