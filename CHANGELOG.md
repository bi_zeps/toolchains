- [gccWin](#gccWin)
- [certGenerator](#certgenerator)

#  gccWin

### 12.2.0-r1
* Alpine 3.18.4
* cmake 3.26.5-r0
* make 4.4.1-r1
* ninja-build 1.11.1-r2
* mingw-w64-gcc 12.2.0-r3

### 10.2.0-r0
* Alpine 3.13.3
* cmake 3.18.4
* make 4.3
* Mingw toolchain 10.2.0
